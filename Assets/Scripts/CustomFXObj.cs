﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomFXObj : MonoBehaviour {

    public GameObject m_Anchor_To;

    public enum ENUM_ANCHOR_BEHAVIOR
    {
        None = 0,
        Follow,
    };
    public ENUM_ANCHOR_BEHAVIOR m_Anchor_Behavior;

    public Vector3 m_Anchor_Offset;

    protected Transform m_AnchorTransform;
    protected Transform m_FXObjTransform;
    protected bool m_FXActivated;

    // Use this for initialization
    void Start () {
		if(m_Anchor_To != null)
        {
            m_AnchorTransform = m_Anchor_To.GetComponent<Transform>();
        }
        m_FXObjTransform = GetComponentInParent<Transform>();
        m_FXActivated = false;
	}
	
	// Update is called once per frame
	void Update () {
        if( m_FXActivated )
        { // Do things
            ParticleSystem part = GetComponent<ParticleSystem>();
            if (part != null)
            {
                if (part.isStopped || part.isPaused)
                {
                    this.DeactivateFX();
                }
            }

            switch (m_Anchor_Behavior )
            {
                case ENUM_ANCHOR_BEHAVIOR.Follow:
                    if( m_AnchorTransform != null )
                    {
                        //Debug.Log("Update for FOLLOW behavior: To " + m_AnchorTransform.position + " with offset " + m_Anchor_Offset );
                        m_FXObjTransform.position = m_AnchorTransform.position + m_Anchor_Offset;
                    }
                    break;
            }
        }		
	}

    public void ActivateFX()
    {
        Debug.Log("Custom FX Activated!");
        m_FXActivated = true;
    }

    public void DeactivateFX()
    {
        Debug.Log("Custom FX Deactivated.");
        m_FXActivated = false;
    }
}
