﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationTrigger : MonoBehaviour {

    protected Animator m_Animator;

	// Use this for initialization
	void Start () {
        m_Animator = GetComponent<Animator>();
    }

    public void OnAnimationEvent(string s)
    {
        string strEvent = s.ToLower();
        Debug.Log("PrintEvent: " + strEvent + " called at: " + Time.time);

        switch (strEvent)
        {
            case "explosion_magic":
                //if (m_External_SFX_Mngr != null)
                //{
                //    m_External_SFX_Mngr.PlaySFX(EffectPlayController.ENUM_SFX.SFX_EXPLOSION_0);
                //}
                Debug.Log("Notify SFX Animation event for explosion ");
                GameObject gameMgr = GameObject.Find("GlobalGameManager");
                if( gameMgr != null )
                {
                    gameMgr.SendMessage("OnCharacterAnimationEvent", "explosion_magic", SendMessageOptions.RequireReceiver);
                    //BroadcastMessage("OnCharacterAnimationEvent", "explosion_magic", SendMessageOptions.RequireReceiver);
                }
                break;
        }
    }
}
