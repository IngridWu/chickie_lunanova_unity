﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveAnimation_Manager : MonoBehaviour {

    protected EffectPlayController  m_External_SFX_Mngr;
    protected Animator m_AvatarAnimator;

    public enum ENUM_TRIGGER_MAGIC_STATE
    {
        TRIG_SFX_IDLE,
        TRIG_SFX_EXPLODE_0,
        TRIG_SFX_STAR_SPARKLE,
        TRIG_SFX_FLOWER_BLOOM,
    };
    protected ENUM_TRIGGER_MAGIC_STATE m_current_state;

// Use this for initialization
void Start () {
        GameObject kSFXContainer = GameObject.Find("/MagicEffects");
        if(kSFXContainer != null)
        {
            Debug.Log("Setup External SFX control...");
            m_External_SFX_Mngr = kSFXContainer.GetComponent<EffectPlayController>();
        }

        GameObject kAvatar = GameObject.Find("/Dummy_PuppetA2D");
        if (kAvatar != null)
        {
            Debug.Log("Setup Character Animator...");
            m_AvatarAnimator = kAvatar.GetComponent<Animator>();
        }
        m_current_state = ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_IDLE;
    }
	
	// Update is called once per frame
	void Update () {
        // Update is called once per frame
        if (Input.GetKeyDown("1"))
        {
            EnterState(ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_EXPLODE_0);
        }
        else if (Input.GetKeyDown("2"))
        {
            EnterState(ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_STAR_SPARKLE);
        }
        else if (Input.GetKeyDown("3"))
        {
            EnterState(ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_FLOWER_BLOOM);
        }
        else if ( m_current_state != ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_IDLE )
        {
            AnimatorClipInfo[] m_AnimatorClipInfo = m_AvatarAnimator.GetCurrentAnimatorClipInfo(0);
            //Debug.Log("Starting clip : " + m_AnimatorClipInfo[0].clip + " with Weight " + m_AnimatorClipInfo[0].weight);
            if(m_AnimatorClipInfo[0].clip.name == "Idle" && m_AnimatorClipInfo[0].weight >= 1.0f )
            {
                EnterState(ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_IDLE);
            }
        }
    }

    protected void EnterState( ENUM_TRIGGER_MAGIC_STATE newState )
    {
        Debug.Log("Entering state " + newState);
        m_current_state = newState;
        switch( m_current_state )
        {
            case ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_EXPLODE_0:
                m_AvatarAnimator.Play("Cast_R");
                break;
            case ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_STAR_SPARKLE:
                m_AvatarAnimator.Play("Cast_UR");
                m_External_SFX_Mngr.PlaySFX(EffectPlayController.ENUM_SFX.SFX_STAR_PARTICLES);
                break;
            case ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_FLOWER_BLOOM:
                m_AvatarAnimator.Play("Cast_L");
                m_External_SFX_Mngr.PlaySFX(EffectPlayController.ENUM_SFX.SFX_FLOWER_BLOOM);
                break;
            case ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_IDLE:
            default:
                break;
        }
    }

    public void OnCharacterAnimationEvent(string s )
    {
        string strEvent = s.ToLower();
        Debug.Log("OnCharacterAnimationEvent: " + strEvent + " called at: " + Time.time);

        switch ( strEvent )
        {
            case "explosion_magic":
                if(m_External_SFX_Mngr != null )
                {
                    m_External_SFX_Mngr.PlaySFX(EffectPlayController.ENUM_SFX.SFX_EXPLOSION_0);
                }
                break;
            default:
                break;
        }
    }

    public void OnButtonClick( string strBtnID )
    {
        Debug.Log("OnButtonClick: " + strBtnID );
        switch( strBtnID )
        {
            case "Flower":
                EnterState(ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_FLOWER_BLOOM);
                break;
            case "Explosion":
                EnterState(ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_EXPLODE_0);
                break;
            case "Sparkles":
                EnterState(ENUM_TRIGGER_MAGIC_STATE.TRIG_SFX_STAR_SPARKLE);
                break;
            default:
                break;
        }
    }
}
