﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectPlayController : MonoBehaviour {
    public Animator m_FX_Explosion_0;
    public ParticleSystem m_FX_StarPartSys;
    public Animator m_FX_FlowerBloom;

    public enum ENUM_SFX
    {
        SFX_NONE = 0,
        SFX_EXPLOSION_0,
        SFX_STAR_PARTICLES,
        SFX_FLOWER_BLOOM,
    };

    // Use this for initialization
    void Start () {
	}
	
    public void PlaySFX( ENUM_SFX eSfx )
    {
        GameObject kFXParent = null;
        switch( eSfx)
        {
            case ENUM_SFX.SFX_EXPLOSION_0:
                // Trigger explosion animation
                m_FX_Explosion_0.SetTrigger("Explode");
                kFXParent = m_FX_Explosion_0.gameObject.transform.parent.gameObject;
                break;
            case ENUM_SFX.SFX_STAR_PARTICLES:
                // Play particle system
                m_FX_StarPartSys.Play(false);
                CustomFXObj kCustomFX = m_FX_StarPartSys.GetComponent<CustomFXObj>();
                if( kCustomFX )
                {
                    kCustomFX.ActivateFX();
                    kFXParent = m_FX_StarPartSys.transform.parent.gameObject;
                }
                break;
            case ENUM_SFX.SFX_FLOWER_BLOOM:
                // Trigger flower bloom animation
                m_FX_FlowerBloom.SetTrigger("Bloom");
                kFXParent = m_FX_FlowerBloom.transform.parent.gameObject;
                break;
        }
        if( kFXParent != null )
        {
            // Attempt to play sound source if it has one
            AudioSource kAudioSrc = kFXParent.GetComponent<AudioSource>();
            if( kAudioSrc != null )
            {
                kAudioSrc.Play();
            }
        }
    }
}
