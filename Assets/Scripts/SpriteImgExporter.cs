﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class SpriteImgExporter : MonoBehaviour {

    public string m_exportFilePath;

	// Use this for initialization
	void Start () {
        if( m_exportFilePath.Length <= 0 )
        { 
            m_exportFilePath = Application.persistentDataPath;
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ExportRigSpriteTextures()
    {
        Component[] arrSpriteRenderers = GetComponentsInChildren< SpriteRenderer >();
        Debug.Log("Found " + arrSpriteRenderers.Length + " sprite renderers for Character Rig");

        foreach (SpriteRenderer kRenderer in arrSpriteRenderers)
        {
            Sprite kSprite = kRenderer.sprite;
            Texture2D tex = kSprite.texture;
            Rect r = kSprite.textureRect;
            Texture2D subtex = CropTexture(tex, (int)r.x, (int)r.y, (int)r.width, (int)r.height);
            byte[] data = subtex.EncodeToPNG();
            File.WriteAllBytes(m_exportFilePath + "/" + kSprite.name + ".png", data);
        }

        Debug.Log("PNG files saved at " + m_exportFilePath);
    }

    public void ExportSpriteSheetAtlas()
    {
        Sprite[] sheet = Resources.LoadAll<Sprite>("Sprites");

        Debug.Log( "Found " + sheet.Length + " sprites for sprite resource");

        foreach (Sprite sprite in sheet)
        {
            Texture2D tex = sprite.texture;
            Rect r = sprite.textureRect;
            Texture2D subtex = CropTexture(tex, (int)r.x, (int)r.y, (int)r.width, (int)r.height);
            byte[] data = subtex.EncodeToPNG();
            File.WriteAllBytes( m_exportFilePath + "/" + sprite.name + ".png", data);
        }

        Debug.Log("PNG files saved at " + m_exportFilePath);
    }

    private static Texture2D CropTexture(Texture2D pSource, int left, int top, int width, int height)
    {
        if (left < 0)
        {
            width += left;
            left = 0;
        }
        if (top < 0)
        {
            height += top;
            top = 0;
        }
        if (left + width > pSource.width)
        {
            width = pSource.width - left;
        }
        if (top + height > pSource.height)
        {
            height = pSource.height - top;
        }

        if (width <= 0 || height <= 0)
        {
            return null;
        }

        Color[] aSourceColor = pSource.GetPixels(0);

        //*** Make New
        Texture2D oNewTex = new Texture2D(width, height, TextureFormat.RGBA32, false);

        //*** Make destination array
        int xLength = width * height;
        Color[] aColor = new Color[xLength];

        int i = 0;
        for (int y = 0; y < height; y++)
        {
            int sourceIndex = (y + top) * pSource.width + left;
            for (int x = 0; x < width; x++)
            {
                aColor[i++] = aSourceColor[sourceIndex++];
            }
        }

        //*** Set Pixels
        oNewTex.SetPixels(aColor);
        oNewTex.Apply();

        //*** Return
        return oNewTex;
    }

}
